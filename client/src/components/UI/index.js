import MyButton from "@/components/UI/MyButton";
import MyInputProductsite from "@/components/UI/MyInputProductsite";
import MyInputProductitem from "@/components/UI/MyInputProductitem";

export default [
    MyButton,
    MyInputProductsite,
    MyInputProductitem,
];
